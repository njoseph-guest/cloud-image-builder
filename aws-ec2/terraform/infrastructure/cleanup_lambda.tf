# AWS Lambda functions larger than 50 MB must be stored in S3
# resource "aws_s3_bucket" "lambda_functions" {
#   bucket = "infrastructure-lambda-functions"
#   tags   = map("Name", "Bucket to store AWS Lambda function packages")
# }


# This block is terraform boilerplate
resource "aws_iam_role" "cleanup_lambda_role" {
  name = "EC2ResourceCleaner"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


data "archive_file" "cleanup" {
  type        = "zip"
  source_file = "${path.module}/cleanup_lambda.py"
  output_path = "${path.module}/files/cleanup_lambda.zip"
}


# Lambda to clean up old AMIs and snapshots
resource "aws_lambda_function" "cleanup_function" {
  function_name    = "cleanup"
  role             = aws_iam_role.cleanup_lambda_role.arn
  handler          = "cleanup_lambda.handler"
  runtime          = "python3.8"
  filename         = "${path.module}/files/cleanup_lambda.zip"
  source_code_hash = data.archive_file.cleanup.output_base64sha256

  tags        = map("Name", "Cleanup Lambda function")
  timeout     = 600
  memory_size = 128
}

resource "aws_iam_policy" "cleanup" {
  description = "Allow cleaning up AMIs and Snapshots"
  name        = "EC2CleanupOperations"
  path        = "/"
  policy = jsonencode(
    {
      Statement = [
        {
          Action = [
            "ec2:DescribeRegions",
            "ec2:DescribeImages",
            "ec2:DescribeImageAttribute",
            "ec2:DeregisterImage",
            "ec2:DescribeSnapshots",
            "ec2:DescribeSnapshotAttribute",
            "ec2:DeleteSnapshot"
          ]
          Effect   = "Allow"
          Resource = "*"
          Sid      = "VisualEditor0"
        },
      ]
      Version = "2012-10-17"
    }
  )
}


resource "aws_iam_role_policy_attachment" "cleanup-attachment" {
  role       = aws_iam_role.cleanup_lambda_role.name
  policy_arn = aws_iam_policy.cleanup.arn
}


resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "cleanup_lambda_logging" {
  role       = aws_iam_role.cleanup_lambda_role.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}


resource "aws_cloudwatch_log_group" "cleanup_lambda_logs" {
  name              = "/aws/lambda/${aws_lambda_function.cleanup_function.function_name}"
  retention_in_days = 14
}
