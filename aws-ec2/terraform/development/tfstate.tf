resource "aws_s3_bucket" "tfstate" {
  bucket = "freedombox-terraform-state-development"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = true
  }

  tags = map("Name", "Development Terraform State")
}


# This table needs to be created only once
resource "aws_dynamodb_table" "tfstate-lock" {
  name           = "terraform-state-lock"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = "Terraform State Lock"
  }
}

