#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
"""
If a new version of the package `freedombox` is uploaded to backports, build a new FreedomBox AMI.
"""

import os
import re
import subprocess

import boto3
import requests
from bs4 import BeautifulSoup


def get_latest_backports_package_version():
    """Return the latest version of backported freedombox package
    by scraping the Debian QA page.
    """
    response = requests.get(
        "https://qa.debian.org/madison.php?package=freedombox")
    version_lines = BeautifulSoup(response.text,
                                  features="html.parser").body.find('pre').text

    for line in version_lines.split('\n'):
        if 'backports' in line:
            return line.split('|')[1].strip()

    return ""


def get_latest_ami_names():
    """Returns the current list of owned AMI names.
    """
    ec2_client = boto3.client('ec2', region_name='eu-central-1')
    images = ec2_client.describe_images(Owners=['self'])
    return [image['Name'] for image in images['Images']]


def compare_versions(version1, version2):
    """Compare version of the format "20.7"
    Had to write this custom function since our versions aren't semver compatible.
    """
    version11, version12 = map(int, version1.split('.'))
    version21, version22 = map(int, version2.split('.'))
    if version11 < version21:
        return -1
    if version11 == version21:
        if version12 < version22:
            return -1
        if version12 == version22:
            return 0
        return 1
    return 1


def is_new_image_required(ami_names, latest_version):
    """Return whether the we have a new backported package to build a new AMI.
    """
    debian_version = latest_version.split('~')[0]
    for ami_name in ami_names:
        matches = re.findall(r'[0-9]+[.][0-9]+', ami_name)
        if matches:
            # AWS API returns the AMIs in the ascending order of creation
            # Comparing only the latest AMI
            latest_ami_version = matches[-1]
            return compare_versions(latest_ami_version, debian_version) == -1
    return False


if __name__ == '__main__':
    AMI_NAMES = get_latest_ami_names()
    LATEST_VERSION = get_latest_backports_package_version()
    os.environ['LATEST_VERSION'] = LATEST_VERSION

    if LATEST_VERSION and is_new_image_required(AMI_NAMES, LATEST_VERSION):
        print("Building AMI for new version of FreedomBox: ", LATEST_VERSION)
        subprocess.run(
            ["packer", "build", "packers/freedombox/freedombox-packer.json"],
            check=True)
    else:
        print("Current FreedomBox AMI is already at latest backports version")
